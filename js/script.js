const div = document.createElement('div');
div.classList.add('container');

const inputTitle = document.createElement('p');
inputTitle.innerText = 'Price:';
inputTitle.classList.add('title');

const input = document.createElement('input');
input.classList.add('input');
input.type = 'number';

div.append(inputTitle, input);

document.body.append(div);

const currentPrice = document.createElement('span');
const button = document.createElement('button');

const notice = document.createElement('p');
notice.innerText = 'Please enter correct price.';
notice.classList.add('notice');


input.addEventListener('focus', (e) => {
    input.style.borderColor = '#90ee90';
    input.style.borderWidth = '2px';
    input.style.borderStyle = 'solid';
});
input.addEventListener('blur', (e) => {
    input.style.borderColor = '#ff0000';
    input.style.borderWidth = '2px';
    input.style.borderStyle = 'solid';

    currentPrice.innerText = `Текущая цена: ${input.value}`;
    currentPrice.classList.add('price');

    button.innerText = 'X';
    input.style.color = '#90ee90';

    if (input.value <= 0) {
        currentPrice.remove();
        button.remove();
        document.body.append(notice);
        input.style.borderColor = '#ff0000';
    } else {
        document.body.prepend(currentPrice, button);
        notice.remove();
        input.style.borderColor = '';
    }
});
button.onclick = () => {
    currentPrice.remove();
    button.remove();
    input.value = '';
}